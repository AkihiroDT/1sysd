#include <stdio.h>
#include <math.h> //pour utiliser la fonction pow() (puissance)
#define PI 3.14159


//Périmètre d'un cercle
long double P(long double r)
{
return 2*PI*r;
}

//Surface d'un disque
long double S(long double r)
{
return PI*pow(r,2);
}


//Test des fonctions
int main() {
long double r, p, s;
printf("Entrer la valeur d'un rayon : ");
scanf("%Lf", &r);
s = S(r);
p = P(r);
printf("Le périmètre du cercle est : %Lf\nLa surface du disque est : %Lf\n", p, s);
}


#include <stdio.h>
#include <stdlib.h>
int factorielle(int n)
{
int resultat;

    if(n<=0)
    {
     return 1;
    }
    else
    {
     for(resultat =1; n > 1; n--)
     {
        resultat *= n;
     }
    }
  
   return resultat;
}


int main(int argc, char *argv[]) {
if (argc > 3) 
{
        printf("Entrer 2 entiers Ex: ./factorial 1 9.\n");
        exit(EXIT_FAILURE);
}
    int start;
    int end;

    start = atoi(argv[1]);
    end = atoi(argv[2]);

    for (int i = start; i <= end; i++) {
        printf("%d! = %d\n", i, factorielle(i));
    }

    return 0;
}

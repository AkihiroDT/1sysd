#include<stdio.h>
#include<stdlib.h>

void display_histogram(int tab[], int size) {
int nb;
    for (int i = 0; i < size; i++) 
	{
	nb = tab[i];
	for (int i=0; i<nb; i++) 
		{
		printf("*");
		}
	printf("\n");
	}
}

int main() {
    int values[] = { 4, 9, 8, 2, 0, 1, 10, 5, 8 };

    display_histogram(values, 9);

    exit(EXIT_SUCCESS);
}

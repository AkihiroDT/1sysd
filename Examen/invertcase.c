#include <stdio.h>
#include <stdlib.h>
void invertcase(char *s) 
{
while (*s) 
	{
	if (*s >='a' && *s <= 'z') 
		{
		*s -=32;
		}
	else if(*s >='A' && *s <= 'Z')
		{
		*s +=32;
		}
	s++;
	}
}

int main(int argc, char *argv[]) 
{
if (argc > 2) 
{
        printf("Une seule chaîne de caractère est attendu en argument.\n");
        exit(EXIT_FAILURE);
}
invertcase(argv[1]);
printf("Votre chaîne inversé => %s\n", argv[1]);
}

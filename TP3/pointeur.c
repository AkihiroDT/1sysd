#include <stdio.h>
#include <stdlib.h>
int slen(char *s)
{
int nc = 0;
while (*s++)
	{
	nc+=1;
	}
return nc;
}
	
//passage des caractères minuscules en majuscules
void supper(char *s) 
{
while (*s) 
	{
	if (*s >='a' && *s <= 'z') 
		{
		*s -=32;
		}
	s++;
	}
}
//l'inverse de supper
void slower(char *s)
{
while (*s)
	{
	if (*s >='A' && *s <= 'Z')
		{
		*s +=32;
		}
	s++;
	}
}
//vérification de l'égalité d'une chaine de caractères
int sequal(char *s1, char *s2) {
     
    while (*s1 != '\0' && *s2 != '\0' && *s2 == *s1) {
        s1++;
        s2++; 
    }
    return *s1 == *s2;
}
//...
char *scopy(char *s)
{
	char *new, *p;
	new = malloc((slen(s) + 1)*sizeof(char));
	p = new;
	while (*s)
	{
	*p++ = *s++;
	}
*p = '\0';
return new;
}
//code d'exemple du prof
int main() {
    char s1[] = "Hello World!";
    char s2[] = "BONJOUR";
    char s3[] = "Bonjour";
    char s4[] = "Bonsoir";
    char s5[] = "Bon";
    char *s6;

    
    if (sequal(s1, s2)) {
        printf("s1 et s2 sont égales\n");
    } else {
        printf("s1 et s2 sont différentes\n");
    }
    if (sequal(s2, s3)) {
        printf("s2 et s3 sont égales\n");
    } else {
        printf("s2 et s3 sont différentes\n");
    }
    if (sequal(s2, s4)) {
        printf("s2 et s4 sont égales\n");
    } else {
        printf("s2 et s4 sont différentes\n");
    }
    if (sequal(s2, s5)) {
        printf("s2 et s5 sont égales\n");
    } else {
        printf("s2 et s5 sont différentes\n");
    }
    supper(s1);
    printf("%s\n", s1);
    slower(s2);
    printf("%s\n", s2);
    printf("Longeur de \"%s\" : %d\n", s1, slen(s1));
    return 0;
}

